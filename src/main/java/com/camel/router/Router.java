package com.camel.router;

import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class Router extends RouteBuilder {

    @Produce
    ProducerTemplate producer;

    ConsumerTemplate consumerTemplate;

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .component("servlet")
                .contextPath("/")
                .bindingMode(RestBindingMode.json);
        
        rest("/crearArchivo")
                .get().route()
                .setHeader(Exchange.FILE_NAME, constant("techie.txt"))
                .to("file:C:\\datafiles\\input");
                
        rest("/deploy/{nombre}")
                .get().route()
                .process((Exchange exchange) -> {
                    consumerTemplate = getContext().createConsumerTemplate();
//                    http://camel.apache.org/file2.html      Decirle que vaya a buscar el archivo que tenga {nombre}
                    Exchange archivo = consumerTemplate.receive("file:C:\\datafiles\\input?noop=true");
                    producer.send("file:C:\\datafiles\\output?autoCreate=true", archivo);
                    consumerTemplate.doneUoW(archivo);
                    consumerTemplate.stop();
                });
//                .to("direct:moverAServidor");

//        from("direct:moverAServidor")
//                http://camel.apache.org/ftp.html
//                .to("sftp://").process((Exchange exchange) -> {
//            exchange.getOut().setBody("Llego al servidor el archivo:" + exchange.getIn().getHeader("CamelFileName", String.class));
//        });

    }

}
